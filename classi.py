class Point2D(object):# object è una classe builtin di python che genera le Classi
#d init serve a inizializzare gli oggetti definiti nella Classe */
	def __init__(self, slot1=None, slot2=None): # __init__ è una funzione che devono avere tutte le classi
		self.x = slot1
		self.y = slot2

	def stampa_x(self):
		print(self.x)


class Point4D(Point2D): #questa classe eredita i metodi della classe genitrice
	def __init__(self, slot2, slot3, newslot4, **newslot5):     #gli asterischi in questo caso denotano la possibiltà che la variabile sia un dizionario */
		super(Point4D, self).__init__(slot1=slot2, slot2=slot3)
		self.z = newslot4
		self.t = newslot5

	def stampa_tempo(self):
		print (self.t)


if __name__ == '__main__': # il doppio underscore significa che il metodo o attributo fa parte della classe 'object'
	Punto2d= Point2D(10,20)
	Punto4D= Point4D(20,30,40, secondi=50)
	print( "onj A", Punto4D.__dict__)
	print ("obj B", Punto2d.__dict__)
	Punto2d.stampa_x()
	Punto4D.stampa_tempo()
	

